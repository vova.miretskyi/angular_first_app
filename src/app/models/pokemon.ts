export type Pokemon = {
  id: number;
  name: string;
  type: string;
  isActive: boolean;
  isStylish: boolean;
  acceptTerms?: boolean;
};

export type PokemonType = {
  key: number;
  value: 'electric' | 'fire' | 'water' | 'rock';
};
