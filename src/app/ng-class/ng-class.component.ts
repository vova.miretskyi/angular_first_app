import { Component } from '@angular/core';

type Pokemons = {
  id: number;
  name: string;
  type: string;
  isActive: boolean;
};

@Component({
  selector: 'app-ng-class',
  templateUrl: './ng-class.component.html',
  styleUrls: ['./ng-class.component.scss'],
})
export class NgClassComponent {
  pokemons: Pokemons[] = [
    {
      id: 1,
      name: 'Pickachu',
      type: 'electric',
      isActive: true,
    },
    {
      id: 2,
      name: 'Charmander',
      type: 'fire',
      isActive: true,
    },
    {
      id: 3,
      name: 'Something-der',
      type: 'water',
      isActive: false,
    },
  ];
}
