import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DataBindingComponent } from './data-binding/data-binding.component';
import { TwoWayDataBindingComponent } from './two-way-data-binding/two-way-data-binding.component';
import { FormsModule } from '@angular/forms';
import { TemplateReferenceVariableComponent } from './template-reference-variable/template-reference-variable.component';
import { NgClassComponent } from './ng-class/ng-class.component';
import { NgStyleComponent } from './ng-style/ng-style.component';
import { NgIfComponent } from './ng-if/ng-if.component';
import { NgForComponent } from './ng-for/ng-for.component';
import { PokemonModule } from './pokemon/pokemon.module';
import { NotFoundComponent } from './not-found/not-found.component';
import { HomeComponent } from './Home/Home.component';
import { NavigationComponent } from './navigation/navigation.component';

@NgModule({
  declarations: [
    AppComponent,
    DataBindingComponent,
    TwoWayDataBindingComponent,
    TemplateReferenceVariableComponent,
    NgClassComponent,
    NgStyleComponent,
    NgIfComponent,
    NgForComponent,
    NotFoundComponent,
    HomeComponent,
    NavigationComponent,
  ],
  imports: [BrowserModule, AppRoutingModule, FormsModule, PokemonModule],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
