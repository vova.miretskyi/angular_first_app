import { Component } from '@angular/core';

type Pokemons = {
  id: number;
  name: string;
};

@Component({
  selector: 'app-ng-for',
  templateUrl: './ng-for.component.html',
  styleUrls: ['./ng-for.component.scss'],
})
export class NgForComponent {
  pokemons: Pokemons[] = [
    {
      id: 1,
      name: 'Pickachu',
    },
    {
      id: 2,
      name: 'Charmander',
    },
    {
      id: 3,
      name: 'Something-der',
    },
  ];
}
