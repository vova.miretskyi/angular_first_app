import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Pokemon } from 'src/app/models/pokemon';

@Component({
  selector: 'app-pokemon-detail',
  templateUrl: './pokemon-detail.component.html',
  styleUrls: ['./pokemon-detail.component.scss'],
})
export class PokemonDetailComponent {
  @Input()
  detail!: Pokemon;

  @Output()
  remove: EventEmitter<Pokemon> = new EventEmitter();

  onRemove() {
    this.remove.emit(this.detail);
  }
}
