import { Component, OnInit } from '@angular/core';
import { PokemonService } from '../../services/pokemon.service';
import { Pokemon, PokemonType } from '../../models/pokemon';
import { ActivatedRoute, Params, Router } from '@angular/router';

@Component({
  selector: 'app-pokemon-template-form',
  templateUrl: './pokemon-template-form.component.html',
  styleUrls: ['./pokemon-template-form.component.css'],
})
export class PokemonTemplateFormComponent implements OnInit {
  pokemon!: Pokemon;
  pokemonTypes: PokemonType[] = [
    {
      key: 0,
      value: 'electric',
    },
    {
      key: 1,
      value: 'water',
    },
    {
      key: 2,
      value: 'fire',
    },
    {
      key: 3,
      value: 'rock',
    },
  ];

  constructor(
    private pokemonService: PokemonService,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  handleSubmit(form: any): void {
    this.pokemonService
      .updatePokemon(this.pokemon)
      .subscribe((data) => (this.pokemon = data));
  }

  ngOnInit() {
    this.route.params.subscribe((data: Params) => {
      this.pokemonService
        .getPokemon(1)
        .subscribe((data) => (this.pokemon = data));
    });
  }

  back(): void {
    this.router.navigate(['/pokemons']);
  }
}
