import {
  AfterViewInit,
  Component,
  ElementRef,
  OnInit,
  Renderer2,
  ViewChild,
  ViewChildren,
} from '@angular/core';
import { Pokemon } from 'src/app/models/pokemon';
import { PokemonService } from 'src/app/services/pokemon.service';

@Component({
  selector: 'app-pokemon-list',
  templateUrl: './pokemon-list.component.html',
  styleUrls: ['./pokemon-list.component.scss'],
})
export class PokemonListComponent implements OnInit, AfterViewInit {
  pokemons!: Pokemon[];
  @ViewChild('headingRef') headingRef!: ElementRef;
  @ViewChildren('pokemonsRef') pokemonsRef!: ElementRef;

  constructor(
    private pokemonService: PokemonService,
    private renderer: Renderer2
  ) {}

  handleRemove(event: Pokemon) {
    this.pokemons = this.pokemons.filter((pokemon) => pokemon.id !== event.id);
  }

  ngOnInit(): void {
    this.pokemonService
      .getPokemons()
      .subscribe((data: Pokemon[]) => (this.pokemons = data));
  }

  ngAfterViewInit(): void {
    // console.log('headingRef', this.headingRef.nativeElement);
    // console.log('pokemonsRef', this.pokemonsRef);
    const div = this.renderer.createElement('div');
    const text = this.renderer.createText('Pokemon List');
    this.renderer.appendChild(div, text);
    this.renderer.appendChild(this.headingRef.nativeElement, div);
  }
}
