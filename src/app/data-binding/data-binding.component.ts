import { Component } from '@angular/core';

@Component({
  selector: 'app-data-binding',
  templateUrl: './data-binding.component.html',
  styleUrls: ['./data-binding.component.scss'],
})
export class DataBindingComponent {
  title: string = 'title binding';
  imgSrc: string =
    'https://upload.wikimedia.org/wikipedia/commons/thumb/5/54/Q_magazine_logo.svg/1200px-Q_magazine_logo.svg.png';
}
