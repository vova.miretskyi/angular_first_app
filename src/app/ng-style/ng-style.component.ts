import { Component } from '@angular/core';
import { Pokemon } from '../models/pokemon';

@Component({
  selector: 'app-ng-style',
  templateUrl: './ng-style.component.html',
  styleUrls: ['./ng-style.component.scss'],
})
export class NgStyleComponent {
  pokemons: Pokemon[] = [
    {
      id: 1,
      name: 'Pickachu',
      type: 'electric',
      isActive: true,
      isStylish: false,
    },
    {
      id: 2,
      name: 'Charmander',
      type: 'fire',
      isActive: true,
      isStylish: false,
    },
    {
      id: 3,
      name: 'Something-der',
      type: 'water',
      isActive: false,
      isStylish: true,
    },
  ];
}
