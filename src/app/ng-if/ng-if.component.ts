import { Component } from '@angular/core';

@Component({
  selector: 'app-ng-if',
  templateUrl: './ng-if.component.html',
  styleUrls: ['./ng-if.component.scss'],
})
export class NgIfComponent {
  pokemonName: string = '';

  handleChange(event: Event) {
    const target = event.target as HTMLInputElement;

    if (target.value || target.value === '') {
      this.pokemonName = target.value;
    }
  }
}
