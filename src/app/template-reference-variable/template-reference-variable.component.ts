import { Component } from '@angular/core';

@Component({
  selector: 'app-template-reference-variable',
  templateUrl: './template-reference-variable.component.html',
  styleUrls: ['./template-reference-variable.component.scss'],
})
export class TemplateReferenceVariableComponent {
  pokemonName: string = '';

  handleClick(pokemonName: string) {
    console.log('pokemonName', pokemonName);
  }
}
