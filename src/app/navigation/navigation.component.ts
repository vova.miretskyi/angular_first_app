import { Component, OnInit } from '@angular/core';

type Nav = {
  link: string;
  name: string;
  exact?: boolean;
};

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.css'],
})
export class NavigationComponent implements OnInit {
  routes: Nav[] = [
    {
      link: '/',
      name: 'Home',
    },
    {
      link: '/pokemons',
      name: 'Pokemons',
    },
  ];

  constructor() {}

  ngOnInit() {}
}
