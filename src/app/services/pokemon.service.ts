import { Injectable } from '@angular/core';
import { Pokemon } from '../models/pokemon';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

const POKEMON_API = 'http://localhost:3000/pokemons';
@Injectable({
  providedIn: 'root',
})
export class PokemonService {
  pokemons!: Pokemon[];

  constructor(private http: HttpClient) {}

  updatePokemon(values: Pokemon) {
    return this.http.put<Pokemon>(`${POKEMON_API}/${values.id}`, { ...values });
  }

  getPokemon(id: number): Observable<Pokemon> {
    return this.http.get<Pokemon>(`${POKEMON_API}/${id}`);
  }

  getPokemons(): Observable<Pokemon[]> {
    return this.http.get<Pokemon[]>(POKEMON_API);
  }
}
